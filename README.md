## Türk Geliştiriciler Topluluğuna HoşGeldiniz 
Selam ben, [kadirselcuk!](https://github.com/kadirselcuk) 👋
---
Github'da açık kaynak yazılımların ve kuruluşların destek verdiği Türk geliştirici topluluklarına katılın! [TurkDevOps!](https://github.com/turkdevops)
--- 
Bizimle geliştirici ekiplerimize katılmak ve destek olmak ister misiniz? [DevTurks!](https://github.com/orgs/turkdevops/teams/devturks-team)
---
Kendinize projeleriniz için TurkDevOps'da Ekip kurun, arkadaşlarınızı ekibinizde yetkilendirin ve çalışmaya başlayın. [Takım Kur!](https://github.com/orgs/turkdevops/teams) )
---
Herkese ücretsiz sonsuza kadar :) 
---
Yeni nesil KİWİIRC canlı sohbet Kanalımızda gelişmeleri yakından takip edin
---
# Bize IRC'de Ulaşın
[![Visit our IRC channel](https://kiwiirc.com/buttons/irc.kiwiirc.com/TurkDevOps.png)](https://kiwiirc.com/client/irc.kiwiirc.com/?nick=DevTurks|?#TurkDevOps)
---
